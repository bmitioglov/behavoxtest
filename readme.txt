* Настройки подключения к БД: BehavoxTestTask\LabelContactApp\src\main\resources\jdbc.properties

* Для запуска необходимы библиотеки из папки BehavoxTestTask\Used libs, 
либо можно использовать готовый bundle BehavoxTestTask\magnolia-5.4.3-labelcontactsapp-bundle

* Приложение должно быть доступно на App launcher страничке, название - Contact Label App

* Дамп БД с тестовыми данными в файле BehavoxTestTask\db_dump.sql


--

* Necessary settings for DB connect: BehavoxTestTask\LabelContactApp\src\main\resources\jdbc.properties

* For launch use libs from BehavoxTestTask\Used libs folder, or you can use ready bundle BehavoxTestTask\magnolia-5.4.3-labelcontactsapp-bundle

* Application should be available on App launcher page, name - Contact Label App

* DB dump with test data located in BehavoxTestTask\db_dump.sql