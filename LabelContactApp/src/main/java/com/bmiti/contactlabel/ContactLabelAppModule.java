package com.bmiti.contactlabel;

import com.bmiti.contactlabel.app.utils.SpringBeanHelper;
import info.magnolia.module.ModuleLifecycle;
import info.magnolia.module.ModuleLifecycleContext;

public class ContactLabelAppModule implements ModuleLifecycle {
    @Override
    public void start(ModuleLifecycleContext moduleLifecycleContext) {
    }

    @Override
    public void stop(ModuleLifecycleContext moduleLifecycleContext) {

    }
}
