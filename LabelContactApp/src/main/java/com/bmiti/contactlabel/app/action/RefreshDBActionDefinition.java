package com.bmiti.contactlabel.app.action;

import info.magnolia.ui.api.action.ConfiguredActionDefinition;

public class RefreshDBActionDefinition extends ConfiguredActionDefinition {

    public RefreshDBActionDefinition() {
        setImplementationClass(RefreshDBAction.class);
    }
}
