package com.bmiti.contactlabel.app.datasource.dao;

import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;

import java.util.List;

public interface ContactDAO {

    public void addContactInfo(ContactInfo contactInfo);

    public List<ContactInfo> listContactInfos();

    public void removeContactInfo(Integer id);

    public ContactInfo getContactInfoById(Integer id);

    public void updateContactInfo(ContactInfo contactInfo);
}
