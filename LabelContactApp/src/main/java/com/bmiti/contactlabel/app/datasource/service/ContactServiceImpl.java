package com.bmiti.contactlabel.app.datasource.service;

import com.bmiti.contactlabel.app.datasource.dao.ContactDAO;
import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {

    @Autowired
    ContactDAO contactDAO;

    public ContactServiceImpl() {

    }

    public void addContactInfo(ContactInfo contactInfo) {
        contactDAO.addContactInfo(contactInfo);
    }

    public List<ContactInfo> listContactsInfo() {
        return contactDAO.listContactInfos();
    }

    public void removeContactInfo(Integer id) {
        contactDAO.removeContactInfo(id);
    }

    public ContactInfo getContactInfoById(Integer id) {
        return contactDAO.getContactInfoById(id);
    }

    public void updateContactInfo(ContactInfo contactInfo) {
        contactDAO.updateContactInfo(contactInfo);
    }
}
