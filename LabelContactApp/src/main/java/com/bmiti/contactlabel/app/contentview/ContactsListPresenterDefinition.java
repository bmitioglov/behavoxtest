package com.bmiti.contactlabel.app.contentview;

import info.magnolia.ui.workbench.list.ListPresenterDefinition;

public class ContactsListPresenterDefinition extends ListPresenterDefinition {

    public ContactsListPresenterDefinition() {
        setImplementationClass(ContactsListPresenter.class);
    }

}
