package com.bmiti.contactlabel.app.action;

import com.bmiti.contactlabel.app.contentconnector.ContactItem;
import info.magnolia.event.EventBus;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.form.EditorCallback;

import javax.inject.Named;

public class AddContactAction<T extends AddContactActionDefinition> extends AbstractAction<T> {
    private ContactItem item;

    private EditorCallback callback;

    private EventBus eventBus;

    public AddContactAction(T definition, ContactItem item, EditorCallback callback,
                               @Named(SubAppEventBus.NAME) EventBus eventBus) {
        super(definition);
        this.item = item;
        this.callback = callback;
        this.eventBus = eventBus;
    }

    @Override
    public void execute() throws ActionExecutionException {
        item.save();
        callback.onSuccess("commit");
    }
}
