package com.bmiti.contactlabel.app.contentconnector;

import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;
import com.bmiti.contactlabel.app.datasource.service.ContactService;
import com.bmiti.contactlabel.app.utils.Constants;
import com.bmiti.contactlabel.app.utils.SpringBeanHelper;
import com.vaadin.data.Property;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ContactContentProperty implements Property.Transactional<ContactInfo>  {
    private static final Logger log = LoggerFactory.getLogger(ContactContentProperty.class);

    private ContactInfo contactInfo;

    private ContactInfo currentValue;
    private ContactInfo bufferedValue;
    private ContactService contactService;

    private boolean transactionInitialized = false;

    public ContactContentProperty(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
        contactService = SpringBeanHelper.getBean(Constants.CONTACT_SERVICE);
    }


    @Override
    public ContactInfo getValue() {
        if (currentValue == null) {
            currentValue = contactInfo;
        }
        return currentValue;
    }

    @Override
    public void setValue(ContactInfo newValue) throws Property.ReadOnlyException {
        if (!transactionInitialized) {
            bufferedValue = getValue();
            transactionInitialized = true;
        }
        this.currentValue = newValue;
    }

    @Override
    public Class<? extends ContactInfo> getType() {
        return ContactInfo.class;
    }

    @Override
    public void startTransaction() {
        transactionInitialized = false;
    }

    @Override
    public void commit() {
        setValue(contactInfo);
        if (transactionInitialized) {
            try {
                contactService.updateContactInfo(contactInfo);
            } catch (Exception e) {
                System.out.println("Error during update contact info");
            } finally {
                startTransaction();
            }
        }
    }

    public void commitRemove() {
            try {
                contactService.removeContactInfo(contactInfo.getId());
            } catch (Exception e) {
                System.out.println("Error during cancel creation of contact info");
            } finally {
                startTransaction();
            }
    }

    @Override
    public void rollback() {
        try {
            if (transactionInitialized) {
                contactService.updateContactInfo(bufferedValue);
            }
        } catch (Exception e) {
            System.out.println("Error during rollback updated contact info");
        } finally {
            startTransaction();
        }
    }

    @Override
    public void setReadOnly(boolean newStatus) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isReadOnly() {
        return false;
    }
}
