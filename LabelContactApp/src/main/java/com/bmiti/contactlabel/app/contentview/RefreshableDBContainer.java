package com.bmiti.contactlabel.app.contentview;


import com.bmiti.contactlabel.app.contentconnector.ContactItem;
import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;
import com.bmiti.contactlabel.app.datasource.service.ContactService;
import com.bmiti.contactlabel.app.utils.Constants;
import com.bmiti.contactlabel.app.utils.SpringBeanHelper;
import com.vaadin.data.Container;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.MethodProperty;
import info.magnolia.ui.workbench.container.Refreshable;


import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.*;

public class RefreshableDBContainer implements Container.ItemSetChangeNotifier, Refreshable {

    private ContactService service;
    private List<ItemSetChangeListener> listeners = new LinkedList<ItemSetChangeListener>();

    private static Method CONTACTITEM_EMAIL;

    private static Method CONTACTITEM_LABEL;

    static {
        try {
            CONTACTITEM_EMAIL = ContactItem.class.getMethod("getEmail");
            CONTACTITEM_LABEL = ContactItem.class.getMethod("getContactLabel");
        } catch (NoSuchMethodException e) {
            System.out.println("No such method in Contact Item class: " + e.getMessage());
        }
    }

    public RefreshableDBContainer() {
        service = SpringBeanHelper.getBean(Constants.CONTACT_SERVICE);
    }

    @Override
    public Item getItem(Object o) {
        ContactInfo info = service.getContactInfoById((Integer) o);
        if (info != null) return new ContactItem(info);
        return null;
    }

    @Override
    public Collection<?> getContainerPropertyIds() {
        List<String> list = new ArrayList<String>();
        list.add("email");
        list.add("contactLabel");
        return list;
    }

    @Override
    public Collection<?> getItemIds() {
        List<Integer> itemIds = new ArrayList<Integer>();
        for (ContactInfo contactInfo : service.listContactsInfo()) {
            itemIds.add(contactInfo.getId());
        }
        return itemIds;
    }

    @Override
    public Property getContainerProperty(Object itemId, Object propertyId) {

        if (!(itemId instanceof Integer)) {
            return null;
        }

        if (propertyId.equals("name")) {
            return new MethodProperty<Object>(getType(propertyId),
                    new ContactItem(service.getContactInfoById((Integer)itemId)), CONTACTITEM_EMAIL, null);
        }

        if (propertyId.equals("email")) {
            return new MethodProperty<Object>(getType(propertyId),
                    new ContactItem(service.getContactInfoById((Integer)itemId)), CONTACTITEM_EMAIL, null);
        }

        if (propertyId.equals("contactLabel")) {
            return new MethodProperty<Object>(getType(propertyId),
                    new ContactItem(service.getContactInfoById((Integer)itemId)), CONTACTITEM_LABEL, null);
        }

        return null;
    }

    @Override
    public Class<?> getType(Object propertyId) {
        if (propertyId.equals("contactLabel")) {
            return String.class;
        }
        if (propertyId.equals("email")) {
            return String.class;
        }
        return null;
    }

    @Override
    public int size() {
        return service.listContactsInfo().size();
    }

    @Override
    public boolean containsId(Object itemId) {
        if (itemId instanceof ContactInfo) {
            ContactInfo info = (ContactInfo) itemId;
            return service.getContactInfoById(info.getId()) != null;
        }
        if (itemId instanceof Integer) {
            return service.getContactInfoById((Integer) itemId) != null;
        }
        return false;
    }

    @Override
    public Item addItem(Object itemId) throws UnsupportedOperationException {
        service.addContactInfo(new ContactInfo(itemId.toString(), "", (Integer) itemId));
        return new ContactItem(service.getContactInfoById((Integer) itemId));
    }

    @Override
    public Object addItem() throws UnsupportedOperationException {
        ContactInfo contactInfo = new ContactInfo();
        service.addContactInfo(contactInfo);
        return contactInfo;
    }

    @Override
    public boolean removeItem(Object itemId) throws UnsupportedOperationException {
        service.removeContactInfo((Integer) itemId);
        return true;
    }

    @Override
    public boolean addContainerProperty(Object o, Class<?> aClass, Object o1)
            throws UnsupportedOperationException {
        throw new UnsupportedOperationException(
                "DB container does not support this operation");
    }

    @Override
    public boolean removeContainerProperty(Object o) throws UnsupportedOperationException {
        throw new UnsupportedOperationException(
                "DB container does not support this operation");
    }

    @Override
    public boolean removeAllItems() throws UnsupportedOperationException {
        for (ContactInfo contactInfo: service.listContactsInfo()) {
            service.removeContactInfo(contactInfo.getId());
        }
        return true;
    }

    @Override
    public void addItemSetChangeListener(ItemSetChangeListener listener) {
        listeners.add(listener);
    }

    @Override
    public void addListener(ItemSetChangeListener listener) {
        listeners.add(listener);
    }

    @Override
    public void removeItemSetChangeListener(ItemSetChangeListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void removeListener(ItemSetChangeListener listener) {
        listeners.remove(listener);
    }

    @Override
    public void refresh() {
        notifyItemSetChange();
    }

    private void notifyItemSetChange() {
        final Object[] l = listeners.toArray();
        BaseItemSetChangeEvent event = new BaseItemSetChangeEvent(this);
        for (int i = 0; i < l.length; i++) {
            ((ItemSetChangeListener) l[i]).containerItemSetChange(event);
        }
    }

    protected static class BaseItemSetChangeEvent extends EventObject implements
            Container.ItemSetChangeEvent, Serializable {

        protected BaseItemSetChangeEvent(Container source) {
            super(source);
        }

        @Override
        public Container getContainer() {
            return (Container) getSource();
        }
    }
}
