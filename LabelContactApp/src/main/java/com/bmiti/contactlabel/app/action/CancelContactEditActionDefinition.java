package com.bmiti.contactlabel.app.action;

import info.magnolia.ui.api.action.ConfiguredActionDefinition;

public class CancelContactEditActionDefinition extends ConfiguredActionDefinition {
    public CancelContactEditActionDefinition() {
        setImplementationClass(CancelContactEditAction.class);
    }
}
