package com.bmiti.contactlabel.app.action;

import info.magnolia.event.EventBus;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.event.AdmincentralEventBus;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;

import javax.inject.Inject;
import javax.inject.Named;

public class RefreshDBAction<T extends RefreshDBActionDefinition> extends AbstractAction<T> {
    private ContentConnector contentConnector;

    private EventBus eventBus;

    @Inject
    public RefreshDBAction(T definition, ContentConnector contentConnector, @Named(AdmincentralEventBus.NAME)EventBus eventBus) {
        super(definition);
        this.contentConnector = contentConnector;
        this.eventBus = eventBus;
    }

    @Override
    public void execute() throws ActionExecutionException {
        eventBus.fireEvent(new ContentChangedEvent(contentConnector.getDefaultItemId()));
    }
}
