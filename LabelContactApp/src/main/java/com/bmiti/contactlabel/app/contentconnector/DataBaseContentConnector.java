package com.bmiti.contactlabel.app.contentconnector;

import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;
import com.bmiti.contactlabel.app.datasource.service.ContactService;
import com.bmiti.contactlabel.app.datasource.service.ContactServiceImpl;
import com.bmiti.contactlabel.app.utils.Constants;
import com.bmiti.contactlabel.app.utils.SpringBeanHelper;
import com.vaadin.data.Item;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.vaadin.integration.contentconnector.AbstractContentConnector;
import info.magnolia.ui.vaadin.integration.contentconnector.SupportsCreation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import java.io.File;
import java.net.URL;
import java.util.List;
import java.util.Random;

public class DataBaseContentConnector extends AbstractContentConnector implements SupportsCreation {
    private Logger log = LoggerFactory.getLogger(DataBaseContentConnector.class);

    private ContactService contactService = SpringBeanHelper.getBean(Constants.CONTACT_SERVICE);
    private DataBaseContentConnectorDefinition definition;

    @Inject
    public DataBaseContentConnector(DataBaseContentConnectorDefinition definition, ComponentProvider componentProvider) {
        super(definition, componentProvider);
        this.definition = definition;
    }

    @Override
    public String getItemUrlFragment(Object itemId) {
        if (itemId instanceof ContactInfo) {
            return ((ContactInfo) itemId).getId().toString();
        }
        return null;
    }

    @Override
    public Object getItemIdByUrlFragment(String urlFragment) {
        return contactService.getContactInfoById(1); //TODO validate
    }

    @Override
    public Item getItem(Object itemId) {
        if (itemId instanceof Integer) {
            return new ContactItem(contactService.getContactInfoById((Integer) itemId));
        }
        if (itemId instanceof ContactInfo) {
            return new ContactItem((ContactInfo) itemId);
        }
        return null;
    }

    @Override
    public Object getItemId(Item item) {
        if (item instanceof ContactItem) {
            return ((ContactItem)item).getContactInfo();
        }
        return null;
    }

    @Override
    public Object getDefaultItemId() {
        return null;
    }

    @Override
    public boolean canHandleItem(Object itemId) {
        return (itemId instanceof ContactInfo);
    }


    @Override
    public Object getNewItemId(Object parentId, Object typeDefinition) {
        Integer id = new Random().nextInt();
        if (id < 0) id = id*(-1);
        contactService.
                addContactInfo(new ContactInfo("empty@test.com", "empty_label", id));
        return contactService.getContactInfoById(id);
    }
}
