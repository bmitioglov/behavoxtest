package com.bmiti.contactlabel.app.datasource.dao;

import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import java.util.List;

@Repository
public class ContactDAOImpl implements ContactDAO {
    
    private EntityManager entityManager;



    @PersistenceUnit
    public void setEntityManagerFactory(EntityManagerFactory emf) {
        entityManager = emf.createEntityManager();
    }

    public void addContactInfo(ContactInfo contactInfo) {
        EntityTransaction tx = entityManager.getTransaction();
        try {
            tx.begin();
            entityManager.persist(contactInfo);
            tx.commit();
        } catch (RuntimeException re) {
            tx.rollback();
            throw re;
        }
    }

    @SuppressWarnings("unchecked")
    public List<ContactInfo> listContactInfos() {
        return entityManager.
                createQuery("from ContactInfo").getResultList();
    }

    public void removeContactInfo(Integer id) {
        EntityTransaction tx = entityManager.getTransaction();
        try {
            tx.begin();
            ContactInfo contactInfo = entityManager.find(ContactInfo.class, id);
            if (contactInfo != null) {
                entityManager.remove(contactInfo);
            }
            tx.commit();
        } catch (RuntimeException re) {
            tx.rollback();
            throw re;
        }
    }

    public ContactInfo getContactInfoById(Integer id) {
        return entityManager.find(ContactInfo.class, id);
    }

    public void updateContactInfo(ContactInfo contactInfo) {
        EntityTransaction tx = entityManager.getTransaction();
        try {
            tx.begin();
            entityManager.merge(contactInfo);
            tx.commit();
        } catch (RuntimeException re) {
            tx.rollback();
            throw re;
        }
    }
}
