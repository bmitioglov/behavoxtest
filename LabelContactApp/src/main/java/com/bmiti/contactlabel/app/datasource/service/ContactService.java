package com.bmiti.contactlabel.app.datasource.service;

import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;

import java.util.List;

public interface ContactService {

    public void addContactInfo(ContactInfo contactInfo);

    public List<ContactInfo> listContactsInfo();

    public void removeContactInfo(Integer id);

    public ContactInfo getContactInfoById(Integer id);

    public void updateContactInfo(ContactInfo info);

}
