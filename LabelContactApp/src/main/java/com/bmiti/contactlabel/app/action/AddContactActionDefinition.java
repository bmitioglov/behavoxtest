package com.bmiti.contactlabel.app.action;


import info.magnolia.ui.api.action.ConfiguredActionDefinition;

public class AddContactActionDefinition extends ConfiguredActionDefinition {
    public AddContactActionDefinition() {
        setImplementationClass(AddContactAction.class);
    }
}
