package com.bmiti.contactlabel.app.action;

import info.magnolia.ui.api.action.ConfiguredActionDefinition;

public class UpdateContactActionDefinition extends ConfiguredActionDefinition {
    public UpdateContactActionDefinition() {
        setImplementationClass(UpdateContactAction.class);
    }
}
