package com.bmiti.contactlabel.app.utils;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class SpringBeanHelper {

    private static volatile ApplicationContext context;

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String beanName) {
        try {
            if (context == null)
                synchronized (SpringBeanHelper.class) {
                    if (context == null) {
                        context = new ClassPathXmlApplicationContext("spring/root-context.xml");
                    }
                }
        } catch (BeansException e) {
            throw new RuntimeException(e);
        }
        return (T) context.getBean(beanName);
    }
}