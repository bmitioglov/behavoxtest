package com.bmiti.contactlabel.app.contentview;

import com.vaadin.data.Container;
import info.magnolia.objectfactory.ComponentProvider;
import info.magnolia.ui.workbench.list.ListPresenter;
import info.magnolia.ui.workbench.list.ListView;

import javax.inject.Inject;

public class ContactsListPresenter extends ListPresenter {

    @Inject
    public ContactsListPresenter(ListView view, ComponentProvider componentProvider) {
        super(view, componentProvider);
    }

    @Override
    public Container initializeContainer() {
        return new RefreshableDBContainer();
    }
}
