package com.bmiti.contactlabel.app.contentconnector;

import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertysetItem;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class ContactItem extends PropertysetItem {
    public static String PROPERTY_EMAIL = "email";

    public static String PROPERTY_LABEL = "contactLabel";

    public static String PROPERTY_NAME = "name";

    public static Collection<String> CONTACT_PROPERTIES;

    static {

        CONTACT_PROPERTIES = new ArrayList<String>();
        CONTACT_PROPERTIES.add(PROPERTY_EMAIL);
        CONTACT_PROPERTIES.add(PROPERTY_LABEL);
        CONTACT_PROPERTIES.add(PROPERTY_NAME);
        CONTACT_PROPERTIES = Collections.unmodifiableCollection(CONTACT_PROPERTIES);
    }

    private ContactContentProperty dataProperty;
    private ObjectProperty<String> email;
    public ObjectProperty<String> contactLabel;
    private ContactInfo contactInfo;

    @SuppressWarnings("unchecked")
    public ContactItem(ContactInfo contactInfo) {
        this.contactInfo = contactInfo;
        if (contactInfo.getLabel() == null) contactInfo.setLabel("");
        if (contactInfo.getEmail() == null) contactInfo.setEmail("");
        this.dataProperty = new ContactContentProperty(contactInfo);
        this.email =  new ObjectProperty<String>(contactInfo.getEmail());
        this.contactLabel = new ObjectProperty<String>(contactInfo.getLabel());

        addItemProperty("data", dataProperty);
        addItemProperty(PROPERTY_LABEL, contactLabel);
        addItemProperty(PROPERTY_EMAIL, email);

        dataProperty.startTransaction();
    }

    public void save() {
        if (!contactInfo.getLabel().equals(contactLabel.getValue()) ||
                !contactInfo.getEmail().equals(email.getValue())){
            contactInfo.setLabel(contactLabel.getValue());
            contactInfo.setEmail(email.getValue());
        }
        dataProperty.commit();
    }

    public void remove() {
        dataProperty.commitRemove();
    }

    public void cancel() {
        dataProperty.rollback();
    }

    public void cancelCreation() {
        dataProperty.commitRemove();
    }

    public ContactInfo getContactInfo() {
        return contactInfo;
    }

    public String getContactLabel() {
        return contactInfo.getLabel();
    }

    public String getEmail() {
        return contactInfo.getEmail();
    }
}
