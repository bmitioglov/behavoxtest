package com.bmiti.contactlabel.app.utils;

public interface Constants {
    String CONTACT_SERVICE = "contactInfoService";
    String CONTACT_DAO = "contactInfoDAO";
}
