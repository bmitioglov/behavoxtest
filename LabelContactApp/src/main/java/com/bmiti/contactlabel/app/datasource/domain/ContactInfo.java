package com.bmiti.contactlabel.app.datasource.domain;

import javax.persistence.*;

@Entity
@Table(name = "CONTACTS")
public class ContactInfo {

    @Id
    @Column(name="ID")
    private Integer id;

    @Column(name = "EMAIL", unique = true)
    private String email;

    @Column(name = "LABEL")
    private String label;

    public ContactInfo(){

    }

    public ContactInfo(String email, String label, Integer id) {
        this.email = email;
        this.label = label;
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}