package com.bmiti.contactlabel.app.action;

import com.bmiti.contactlabel.app.contentconnector.ContactItem;
import com.bmiti.contactlabel.app.datasource.domain.ContactInfo;
import info.magnolia.event.EventBus;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.api.event.AdmincentralEventBus;
import info.magnolia.ui.api.event.ContentChangedEvent;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.vaadin.integration.contentconnector.ContentConnector;

import javax.inject.Named;

public class RemoveContactAction<T extends RemoveContactActionDefinition> extends AbstractAction<T> {
    private ContactItem item;
    private EventBus eventBus;
    private ContentConnector contentConnector;

    public RemoveContactAction(T definition, ContentConnector contentConnector, ContactItem item,
                               @Named(AdmincentralEventBus.NAME) final EventBus eventBus) {
        super(definition);
        this.item = item;
        this.eventBus = eventBus;
        this.contentConnector = contentConnector;
    }

    @Override
    public void execute() throws ActionExecutionException {
        ContactInfo info = new ContactInfo(item.getEmail(), item.getContactLabel(), item.getContactInfo().getId());
        item.remove();
        eventBus.fireEvent(new ContentChangedEvent(info));
    }
}
