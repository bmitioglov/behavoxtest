package com.bmiti.contactlabel.app.action;

import com.bmiti.contactlabel.app.contentconnector.ContactItem;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.form.EditorCallback;

public class CancelContactEditAction<T extends CancelContactEditActionDefinition> extends AbstractAction<T> {

    private ContactItem item;

    private EditorCallback editorCallback;

    public CancelContactEditAction(T definition, ContactItem item, EditorCallback editorCallback) {
        super(definition);
        this.item = item;
        this.editorCallback = editorCallback;
    }

    @Override
    public void execute() throws ActionExecutionException {
        item.cancel();
        editorCallback.onCancel();
    }
}
