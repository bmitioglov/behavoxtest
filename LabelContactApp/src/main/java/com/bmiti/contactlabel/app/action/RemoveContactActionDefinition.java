package com.bmiti.contactlabel.app.action;

import info.magnolia.ui.api.action.ConfiguredActionDefinition;

public class RemoveContactActionDefinition extends ConfiguredActionDefinition {
    public RemoveContactActionDefinition() {
        setImplementationClass(RemoveContactAction.class);
    }
}