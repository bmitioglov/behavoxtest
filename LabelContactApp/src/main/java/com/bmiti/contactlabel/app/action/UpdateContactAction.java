package com.bmiti.contactlabel.app.action;

import com.bmiti.contactlabel.app.contentconnector.ContactItem;
import info.magnolia.event.EventBus;
import info.magnolia.ui.api.action.AbstractAction;
import info.magnolia.ui.api.action.ActionExecutionException;
import info.magnolia.ui.api.app.SubAppEventBus;
import info.magnolia.ui.form.EditorCallback;
import info.magnolia.ui.workbench.event.SelectionChangedEvent;

import javax.inject.Named;
import java.util.HashSet;
import java.util.Set;

public class UpdateContactAction<T extends UpdateContactActionDefinition> extends AbstractAction<T> {
    protected ContactItem item;

    protected EditorCallback callback;

    protected EventBus eventBus;

    public UpdateContactAction(T definition, ContactItem item, EditorCallback callback,
                               @Named(SubAppEventBus.NAME) EventBus eventBus) {
        super(definition);
        this.item = item;
        this.callback = callback;
        this.eventBus = eventBus;
    }

    @Override
    public void execute() throws ActionExecutionException {
        item.save();
        callback.onSuccess("commit");
        Set<Object> ids = new HashSet<Object>();
        ids.add(item.getContactInfo());
        eventBus.fireEvent(new SelectionChangedEvent(ids));
    }
}
