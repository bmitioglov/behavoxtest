package com.bmiti.contactlabel.app.contentconnector;

import info.magnolia.ui.vaadin.integration.contentconnector.ConfiguredContentConnectorDefinition;

public class DataBaseContentConnectorDefinition extends ConfiguredContentConnectorDefinition {

    public DataBaseContentConnectorDefinition() {
        setImplementationClass(DataBaseContentConnector.class);
    }

}
