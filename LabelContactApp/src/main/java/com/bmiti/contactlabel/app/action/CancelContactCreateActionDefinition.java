package com.bmiti.contactlabel.app.action;

import info.magnolia.ui.api.action.ConfiguredActionDefinition;

public class CancelContactCreateActionDefinition extends ConfiguredActionDefinition {
    public CancelContactCreateActionDefinition() {
        setImplementationClass(CancelContactCreateAction.class);
    }
}
